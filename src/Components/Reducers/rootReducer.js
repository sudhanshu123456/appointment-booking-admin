import { combineReducers } from "redux";

const DEFAULT_REDUCER = (initstate, action) => {
  return {
    DEFAULT: "HELLO WORLD",
  };
};

const rootReducer = combineReducers({
  key: DEFAULT_REDUCER,
});

export default rootReducer;
