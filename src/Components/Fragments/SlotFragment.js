import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Moment from "react-moment";

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

export class SlotFragment extends Component {
  constructor() {
    super();
    this.state = { slots: [], branches: [] };
    //this.state = { branches: [] };
    console.log("test");
  }

  componentDidMount() {
    fetch("http://localhost:8080/api/slots")
      .then((res) => res.json())
      .then((data) => {
        this.setState({ slots: data });
      })
      .catch(console.log);
    fetch("http://localhost:8080/api/branches")
      .then((res) => res.json())
      .then((data1) => {
        this.setState({ branches: data1 });
      })
      .catch(console.log);
  }
  render() {
    //console.log(branches);
    return (
      <TableContainer>
        <Table>
          <TableHead>
            <TableRow>
              <StyledTableCell align="center">S.NO</StyledTableCell>
              <StyledTableCell align="center">Branch</StyledTableCell>
              <StyledTableCell align="center">Slot</StyledTableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {this.state.slots.map((row, index) => (
              <StyledTableRow key={row._id}>
                <StyledTableCell align="center">{index + 1}</StyledTableCell>
                <StyledTableCell align="center">
                  {this.state.branches.map((row_branch) =>
                    row.branch == row_branch._id ? row_branch.name : ""
                  )}
                </StyledTableCell>
                <StyledTableCell align="center">
                  <Moment unix>{row.time}</Moment>
                </StyledTableCell>
              </StyledTableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    );
  }
}

export default SlotFragment;
