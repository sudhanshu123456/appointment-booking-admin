import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import AppBar from "@material-ui/core/AppBar";
import CssBaseline from "@material-ui/core/CssBaseline";
import Toolbar from "@material-ui/core/Toolbar";
import List from "@material-ui/core/List";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import {
  Home,
  CalendarToday,
  AccountBalance,
  MemoryRounded,
} from "@material-ui/icons";
import Dashboard from "./pages/Dashboard";
import AppointmentFragment from "./Components/Fragments/AppointmentFragment";
import SlotFragment from "./Components/Fragments/SlotFragment";

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  drawerContainer: {
    overflow: "auto",
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
}));

export default function ClippedDrawer() {
  const classes = useStyles();
  const [component, setComponent] = useState("user");
  let comp;
  switch (component) {
    case "dashboard":
      comp = <Dashboard />;
      break;

    case "appointment":
      comp = <AppointmentFragment />;
      break;

    case "slot":
      comp = <SlotFragment />;
      break;

    default:
      comp = <Dashboard />;
  }
  // if (component === "appointment") {
  //   comp = <AppointmentFragment />;
  // } else if (component === "dashboard") {
  //   comp = <Dashboard />;
  // } else if (component === "slot") {
  //   comp = <BranchFragment />;
  // } else {
  //   comp = <Dashboard />;
  // }

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar>
          <Typography variant="h6" noWrap>
            HLB Appointment Booking Admin
          </Typography>
        </Toolbar>
      </AppBar>
      <Drawer
        className={classes.drawer}
        variant="permanent"
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        <Toolbar />
        <div className={classes.drawerContainer}>
          <List>
            <ListItem button onClick={() => setComponent("dashboard")}>
              <ListItemIcon>
                <Home />
              </ListItemIcon>
              <ListItemText primary="Appointment" />
            </ListItem>
            {/* <ListItem button onClick={() => setComponent("appointment")}>
              <ListItemIcon>
                <CalendarToday />
              </ListItemIcon>
              <ListItemText primary="Appointment Dates" />
            </ListItem> */}
            <ListItem button onClick={() => setComponent("branch")}>
              <ListItemIcon>
                <AccountBalance />
              </ListItemIcon>
              <ListItemText primary="Branches" />
            </ListItem>
            <ListItem button onClick={() => setComponent("slot")}>
              <ListItemIcon>
                <MemoryRounded />
              </ListItemIcon>
              <ListItemText primary="Slots" />
            </ListItem>
          </List>
          <Divider />
        </div>
      </Drawer>
      <main className={classes.content}>
        <Toolbar />
        <div className={classes.toolbar} />
        {comp}
        {/* {component === "appointment" ? <AppointmentFragment /> : <Dashboard />} */}
      </main>
    </div>
  );
}
