import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import Moment from "react-moment";

import TextField from "@material-ui/core/TextField";
import { Row } from "antd";

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

/*function createData(name, calories, fat) {
  return { name, calories, fat };
}

const rows = [
  createData("Customer name", 159, 6.0),
  createData("Customer email", 237, 9.0),
  createData("Phone", 262, 16.0),
];*/

export class Dashboard extends Component {
  state = {
    query: "",
    data: [],
    filteredData: [],
  };

  handleInputChange = (event) => {
    const query = event.target.value;
    const input = event.target.name;
    if (input === "select") {
      this.setState((prevState) => {
        const filteredData = prevState.data.filter((element) => {
          return element.slot.branch.name
            .toLowerCase()
            .includes(query.toLowerCase());
        });

        return {
          query,
          filteredData,
        };
      });
    } else {
      this.setState((prevState) => {
        const filteredData = prevState.data.filter((element) => {
          return (
            element.slot.branch.name
              .toLowerCase()
              .includes(query.toLowerCase()) ||
            element.name.toLowerCase().includes(query.toLowerCase()) ||
            element.email.toLowerCase().includes(query.toLowerCase()) ||
            element.phone.includes(query)
          );
        });

        return {
          query,
          filteredData,
        };
      });
    }
  };

  getData = () => {
    fetch("http://localhost:8080/api/appointments")
      .then((response) => response.json())
      .then((data) => {
        const { query } = this.state;
        const filteredData = data.filter((element) => {
          return element.slot.branch.name
            .toLowerCase()
            .includes(query.toLowerCase());
        });

        this.setState({
          data,
          filteredData,
        });
      });
  };

  componentDidMount() {
    Moment.globalFormat = "D MMM YYYY hh:mm a";
    this.getData();
  }

  render() {
    return (
      <div>
        <div style={{ flex: Row, flexDirection: Row }}>
          <FormControl style={{ width: 150, flex: Row, flexDirection: Row }}>
            <InputLabel
              style={{ width: 150 }}
              id="demo-simple-select-required-label"
            >
              Select Branch
            </InputLabel>
            <Select
              value={this.state.query}
              name={"select"}
              onChange={this.handleInputChange}
            >
              <MenuItem value="">
                <em>None</em>
              </MenuItem>
              <MenuItem value="Damansara">Damansara</MenuItem>
              <MenuItem value="Puchong">Puchong</MenuItem>
              <MenuItem value="Cheras">Cheras</MenuItem>
              <MenuItem value="Johor">Johor</MenuItem>
            </Select>
          </FormControl>
          <TextField
            style={{ marginLeft: 20 }}
            id="outlined-helperText"
            label="Search ...."
            defaultValue=""
            helperText=""
            variant="outlined"
            value={this.state.query}
            onChange={this.handleInputChange}
          />
        </div>
        <div style={{ paddingTop: 20, paddingRight: 0 }}>
          <TableContainer component={Paper}>
            <Table aria-label="customized table">
              <TableHead>
                <TableRow>
                  <StyledTableCell>Customer Name</StyledTableCell>
                  <StyledTableCell>Customer Email</StyledTableCell>
                  <StyledTableCell>Phone</StyledTableCell>
                  <StyledTableCell>Branch</StyledTableCell>
                  <StyledTableCell>Time</StyledTableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {this.state.filteredData.map((dynamicData) => (
                  <StyledTableRow key={dynamicData._id}>
                    <StyledTableCell>{dynamicData.name}</StyledTableCell>
                    <StyledTableCell>{dynamicData.email}</StyledTableCell>
                    <StyledTableCell>{dynamicData.phone}</StyledTableCell>
                    <StyledTableCell>
                      {dynamicData.slot.branch.name}
                    </StyledTableCell>
                    <StyledTableCell>
                      <Moment unix>{dynamicData.slot.time}</Moment>
                    </StyledTableCell>
                  </StyledTableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </div>
      </div>
    );
  }
}

export default Dashboard;
